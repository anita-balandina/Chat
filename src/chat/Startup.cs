﻿    using System.IO;
using chat.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace chat
{
    public class Startup
    {
        public Startup( IHostingEnvironment env )
        {
            var builder =
                new ConfigurationBuilder().SetBasePath( env.ContentRootPath )
                    .AddJsonFile( "appsettings.json", true, true )
                    .AddJsonFile( $"appsettings.{env.EnvironmentName}.json", true )
                    .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        private IConfigurationRoot Configuration { get; }

        public void ConfigureServices( IServiceCollection services )
        {
            services.AddDbContext< SiteDbContext >( options => options.UseSqlServer( Configuration.GetConnectionString( "DefaultConnection" ) ) )
                .AddMvc();
            services.AddIdentity< User, IdentityRole >( options =>
            {
                options.Password.RequireDigit =
                    options.Password.RequireLowercase = options.Password.RequireNonAlphanumeric = options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 5;
            } ).AddEntityFrameworkStores< SiteDbContext >().AddDefaultTokenProviders();
        }

        public void Configure( IApplicationBuilder app, ILoggerFactory loggerFactory )
        {
            loggerFactory.AddConsole( Configuration.GetSection( "Logging" ) ).AddDebug();
            app.UseBrowserLink().UseDeveloperExceptionPage().UseDatabaseErrorPage();
            app.UseIdentity()
                .UseStaticFiles()
                .UseMvc( routes => routes.MapRoute( "default", "{controller=Home}/{action=Index}/{id?}" ) )
                .InitializeDbAsync();
        }

        public static void Main( string[] args )
        {
            var host =
                new WebHostBuilder().UseKestrel()
                    .UseContentRoot( Directory.GetCurrentDirectory() )
                    .UseIISIntegration()
                    .UseStartup< Startup >()
                    .Build();

            host.Run();
        }
    }
}