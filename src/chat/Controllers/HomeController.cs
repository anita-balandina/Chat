﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace chat.Controllers
{
    [Authorize]
    public sealed class HomeController : Controller
    {
        public IActionResult Index() => View();

        public IActionResult AddMessage()
        {
            return null;
        }
    }
}