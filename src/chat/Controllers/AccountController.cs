﻿using System.Threading.Tasks;
using chat.Models;
using chat.Models.AccountViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace chat.Controllers
{
    public sealed class AccountController : Controller
    {
        private readonly SignInManager< User > signInManager;
        private readonly UserManager< User > userManager;

        public AccountController( UserManager< User > userManager, SignInManager< User > signInManager )
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task< IActionResult > Login( LoginViewModel model )
        {
            if ( !ModelState.IsValid )
                return View( model );
            var result = await signInManager.PasswordSignInAsync( model.Email, model.Password, false, false );
            if ( result.Succeeded )
                return RedirectToChat();
            ModelState.AddModelError( string.Empty, "Invalid login attempt." );
            return View( model );
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task< IActionResult > Register( RegisterViewModel model, string returnUrl = null )
        {
            ViewData[ "ReturnUrl" ] = returnUrl;
            if ( !ModelState.IsValid )
                return View( model );
            var user = new User { UserName = model.Email, Email = model.Email };
            var result = await userManager.CreateAsync( user, model.Password );
            if ( result.Succeeded )
            {
                await signInManager.SignInAsync( user, false );
                return RedirectToChat();
            }
            AddErrors( result );
            return View( model );
        }

        [HttpPost]
        [Authorize]
        public async Task< IActionResult > LogOff()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction( "Login" );
        }

        private void AddErrors( IdentityResult result )
        {
            foreach ( var error in result.Errors )
                ModelState.AddModelError( string.Empty, error.Description );
        }

        private IActionResult RedirectToChat() => RedirectToAction( nameof( HomeController.Index ), "Home" );
    }
}