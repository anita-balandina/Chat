﻿using System.ComponentModel.DataAnnotations;

namespace chat.Models.AccountViewModels
{
    public sealed class LoginViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType( DataType.Password )]
        public string Password { get; set; }
    }
}