﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace chat.Models
{
    public sealed class SiteDbContext : IdentityDbContext< User >
    {
        public SiteDbContext()
        {
        }

        public SiteDbContext( DbContextOptions options ) : base( options )
        {
        }
    }
}