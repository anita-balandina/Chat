﻿namespace chat.Models
{
    public sealed class ChatMessage
    {
        public string Message { get; set; }
    }
}