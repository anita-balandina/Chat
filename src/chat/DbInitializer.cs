﻿using chat.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace chat
{
    internal static class DbInitializer
    {
        internal static async void InitializeDbAsync( this IApplicationBuilder app )
        {
            await app.ApplicationServices.GetService< SiteDbContext >().Database.EnsureCreatedAsync();
        }
    }
}